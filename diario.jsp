<%-- 
    Document   : diario
    Created on : 16-may-2020, 23:49:22
    Author     : PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"
import ="java.sql.*"              
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection conex = (Connection)DriverManager.getConnection("jdbc:mysql://127.0.0.1/p101?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false","root","1qaz3edc.");
        Statement sql = conex.createStatement();
        String qry = "select * from textos";
        ResultSet data = sql.executeQuery(qry);
        %>
        <div style="text-align:center;">
            
            <h1>Diario</h1>
            <hr>
            <a href="agregar.jsp" >agregar</a>
            <a href="editar.jsp">editar</a>
            <a href="cal.jsp">calificar</a>
            <a href="eliminar.jsp">eliminar</a>
            <br>
             <h2 style="text-align:center;">Registros</h2>
            <table border ="1" style="text-align:center;"  align="center">
                <tr>
                    <th>ID</th>
                    <th>TXT</th>
                    <th>FECHA</th>
                    <th>HORA</th>
                    <th>CALIFICACION</th>            
                </tr>
                <%
                   while(data.next()){
                       if(data.getInt("eliminar")!=1){
                %>
                <tr style="visibility:collapse" >
                    <%               
                 }
                %>
                    
                    <td> <% out.print(data.getInt("id"));%></td>
                    <td> <% out.print(data.getString("texto"));%></td>
                    <td> <% out.print(data.getString("fecha"));%></td>
                    <td> <% out.print(data.getString("hora"));%></td>
                    <td> <% out.print(data.getString("calificacion"));%></td>
                    
                </tr>
                <%               
                 }
                %>
            </table>
            
            
        </div>
    </body>
</html>
